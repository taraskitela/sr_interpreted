﻿    using NAND_Prog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR_Interpreted
{
    [Export(typeof(Function)),
             ExportMetadata("Name", "SR_Interpreted")]
    public class Implement : Function
    {
        #region Infrastructure Support

        public Func<Operation, SRregister, VerifyResult> Implementation;

        public Implement()
        {
            Implementation = Interpreted;
        }

        public override Type ImplementaionType()
        {
            return Implementation.GetType();
        }
        public override Delegate GetImplementation()
        {
            return Implementation;
        }

        #endregion

        //------------------------------------------

        
        public VerifyResult Interpreted(Operation operation, SRregister register)

        {
            //if (((operation == OperationType.NotDefined) || (operation == OperationType.NOP)))    // Якщо операція ні  Program Erase Read
            //    return;
            //if (value == -1)
            //    return;
            int IO = 0;      // Значучий біт для перевірки результата операції operation
            int value;       // Значення біта в позиції IO

            BitArray bite_array = new BitArray(register.GetContent());
            if (bite_array.Get(IO))
                value = 1;
            else value = 0;



            bool BlockErase = false, Read = false, PageProgram = false;

            if (operation is AbstractErase)
                BlockErase = true;
            if (operation is AbstractProgramm)
                PageProgram = true;
            if (operation is AbstractRead)
                Read = true;


            //    //Все що за межами регіону User Code  буде сховано від користувача

            #region Help
            //    // Note:
            //    // strResult - string representation of the result (from Datasheet) .
            //    // bResult - logical representation of the result . May be true when result is OK or false when is not OK or null when not use .
            //    //
            //    //------------For exemple (K9F2G08X0B.pdf page 38) ---------
            //    /*
            //    Table 3. Read Status Register Definition for 70h Command
            //    -------------------------------------------------------------------------------------------------
            //    | I/O    | Page Program     | Block Erase   | Read         | Definition   
            //    -------------------------------------------------------------------------------------------------
            //    | I/O 0  |  Pass/Fail       | Pass/Fail     | Not use      | Pass : "0" Fail : "1"
            //    | I/O 1  |  Not use         | Not use       | Not use      | Don’t -cared
            //    | I/O 2  |  Not use         | Not use       | Not use      | Don’t -cared
            //    | I/O 3  |  Not Use         | Not Use       | Not Use      | Don’t -cared
            //    | I/O 4  |  Not Use         | Not Use       | Not Use      | Don’t -cared
            //    | I/O 5  |  Not Use         | Not Use       | Not Use      | Don’t -cared
            //    | I/O 6  |  Ready/Busy      | Ready/Busy    | Ready/Busy   | Busy : "0" Ready : "1"
            //    | I/O 7  |  Write Protect   | Write Protect | Write Protect| Protected : "0" Not Protected : "1"
            //    --------------------------------------------------------------------------------------------------

            //     NOTE : I/Os defined ’Not use’ are recommended to be masked out when Read Status is being executed.

            //    //-----------------------Code for table 3----------------------------


            //    //Begin Code
            //    switch (IO)
            //    {
            //        case 0:

            //            if (PageProgram) { strResult = value == 0 ? "Pass" : "Fail";    bResult = value == 0 ? true : false; }
            //            if (BlockErase)  { strResult = value == 0 ? "Pass" : "Fail";    bResult = value == 0 ? true : false; }
            //            if (Read)        { strResult = "Note Use";  bResult = null; }

            //            break;
            //        case 1:

            //            if (PageProgram) { strResult = "Note Use"; bResult = null; }
            //            if (BlockErase)  { strResult = "Note Use"; bResult = null; }
            //            if (Read)        { strResult = "Note Use"; bResult = null; }

            //            break;
            //        case 2:

            //            if (PageProgram) { strResult = "Note Use"; bResult = null; }
            //            if (BlockErase)  { strResult = "Note Use"; bResult = null; }
            //            if (Read)        { strResult = "Note Use"; bResult = null; }

            //            break;
            //        case 3:

            //            if (PageProgram) { strResult = "Note Use"; bResult = null; }
            //            if (BlockErase)  { strResult = "Note Use"; bResult = null; }
            //            if (Read)        { strResult = "Note Use"; bResult = null; }

            //            break;
            //        case 4:

            //            if (PageProgram) { strResult = "Note Use"; bResult = null; }
            //            if (BlockErase)  { strResult = "Note Use"; bResult = null; }
            //            if (Read)        { strResult = "Note Use"; bResult = null; }

            //            break;
            //        case 5:

            //            if (PageProgram) { strResult = "Note Use"; bResult = null; }
            //            if (BlockErase)  { strResult = "Note Use"; bResult = null; }
            //            if (Read)        { strResult = "Note Use"; bResult = null; }

            //            break;
            //        case 6:

            //            if (PageProgram) { strResult = value == 0 ? "Busy" : "Ready"; bResult = value == 1 ? true : false; }
            //            if (BlockErase)  { strResult = value == 0 ? "Busy" : "Ready"; bResult = value == 1 ? true : false; }
            //            if (Read)        { strResult = value == 0 ? "Busy" : "Ready"; bResult = value == 1 ? true : false; }

            //            break;
            //        case 7:

            //            if (PageProgram) { strResult = value == 0 ? "Write Protect" : "Not Write Protect"; bResult = value == 1 ? true : false; }
            //            if (BlockErase)  { strResult = value == 0 ? "Write Protect" : "Not Write Protect"; bResult = value == 1 ? true : false; }
            //            if (Read)        { strResult = value == 0 ? "Write Protect" : "Not Write Protect"; bResult = value == 1 ? true : false; }

            //            break;
            //    }
            //    //End Code

            //    You can use the code between tags //Begin Code and //End Code as a template .

            //    */
            #endregion

            #region  User Code
           
            switch (IO)
            {
                case 0:

                    if (PageProgram) { register.strResult = value == 0 ? "Pass" : "Fail"; register.bResult = value == 0 ? true : false; }
                    if (BlockErase) { register.strResult = value == 0 ? "Pass" : "Fail"; register.bResult = value == 0 ? true : false; }
                    if (Read) { register.strResult = "Note Use"; register.bResult = null; }

                    break;
                case 1:

                    if (PageProgram) { register.strResult = "Note Use"; register.bResult = null; }
                    if (BlockErase) { register.strResult = "Note Use"; register.bResult = null; }
                    if (Read) { register.strResult = "Note Use"; register.bResult = null; }

                    break;
                case 2:

                    if (PageProgram) { register.strResult = "Note Use"; register.bResult = null; }
                    if (BlockErase) { register.strResult = "Note Use"; register.bResult = null; }
                    if (Read) { register.strResult = "Note Use"; register.bResult = null; }

                    break;
                case 3:

                    if (PageProgram) { register.strResult = "Note Use"; register.bResult = null; }
                    if (BlockErase) { register.strResult = "Note Use"; register.bResult = null; }
                    if (Read) { register.strResult = "Note Use"; register.bResult = null; }

                    break;
                case 4:

                    if (PageProgram) { register.strResult = "Note Use"; register.bResult = null; }
                    if (BlockErase) { register.strResult = "Note Use"; register.bResult = null; }
                    if (Read) { register.strResult = "Note Use"; register.bResult = null; }

                    break;
                case 5:

                    if (PageProgram) { register.strResult = "Note Use"; register.bResult = null; }
                    if (BlockErase) { register.strResult = "Note Use"; register.bResult = null; }
                    if (Read) { register.strResult = "Note Use"; register.bResult = null; }

                    break;
                case 6:

                    if (PageProgram) { register.strResult = value == 0 ? "Busy" : "Ready"; register.bResult = value == 1 ? true : false; }
                    if (BlockErase) { register.strResult = value == 0 ? "Busy" : "Ready"; register.bResult = value == 1 ? true : false; }
                    if (Read) { register.strResult = value == 0 ? "Busy" : "Ready"; register.bResult = value == 1 ? true : false; }

                    break;
                case 7:

                    if (PageProgram) { register.strResult = value == 0 ? "Write Protect" : "Not Write Protect"; register.bResult = value == 1 ? true : false; }
                    if (BlockErase) { register.strResult = value == 0 ? "Write Protect" : "Not Write Protect"; register.bResult = value == 1 ? true : false; }
                    if (Read) { register.strResult = value == 0 ? "Write Protect" : "Not Write Protect"; register.bResult = value == 1 ? true : false; }

                    break;
            }

            if (register.bResult == true)
                return VerifyResult.Pass;
            else if (register.bResult == false)
                return VerifyResult.Fail;
            else 
                return VerifyResult.NoteUse;


            //return register.bResult;

            #endregion


            //}


        }

        
    }
}
